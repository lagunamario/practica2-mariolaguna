﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion
{
    class Program
    {
        static void Main(string[] args)
        {

            String[] v = new String[5];
            int opcion;

            do
            {
                Console.WriteLine(" *** MENU ***");
                Console.WriteLine(" 1) Rellenar vector");
                Console.WriteLine(" 2) Visualizar vector");
                Console.WriteLine(" 3) Buscar valor en el vector");
                Console.WriteLine(" 4) Suma dos valores");
                Console.WriteLine("Opcion: ");
                opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {

                    case 1:
                        Libreria.Class1.rellenarVector(v);
                        break;

                    case 2:
                        Libreria.Class1.visualizarVector(v);
                        break;

                    case 3:
                        Libreria.Class1.buscarValoresEnVector(v);
                        break;

                    case 4:
                        Libreria.Class1.suma(4, 6);
                        break;

                    default:
                        Console.WriteLine("No es un numero correcto.");
                        break;
                }

            } while (opcion != 4);
        }
    }
}
