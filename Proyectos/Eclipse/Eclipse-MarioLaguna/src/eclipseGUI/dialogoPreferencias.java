package eclipseGUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JCheckBox;
import java.awt.Toolkit;

/**
 * 
 * @author Mario
 * @since 16/01/2019
 */
public class dialogoPreferencias extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			dialogoPreferencias dialog = new dialogoPreferencias();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public dialogoPreferencias() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Mario\\Pictures\\usb.PNG"));
		setTitle("Ejercicio 2. Preferencias\r\n");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			textField = new JTextField();
			textField.setBounds(10, 11, 86, 20);
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JTextArea textArea = new JTextArea();
			textArea.setBounds(10, 37, 86, 180);
			contentPanel.add(textArea);
		}
		{
			JLabel lblTemplates = new JLabel("Plantillas de configuracion");
			lblTemplates.setBounds(106, 14, 168, 14);
			contentPanel.add(lblTemplates);
		}
		{
			JLabel lblCreate = new JLabel("Crea, edita o elimina plantillas");
			lblCreate.setFont(new Font("Tahoma", Font.PLAIN, 9));
			lblCreate.setBounds(106, 37, 195, 14);
			contentPanel.add(lblCreate);
		}
		{
			JTextArea textArea = new JTextArea();
			textArea.setBounds(106, 62, 227, 155);
			contentPanel.add(textArea);
		}
		{
			JButton btnNewButton = new JButton("Nuevo");
			btnNewButton.setBounds(343, 63, 81, 23);
			contentPanel.add(btnNewButton);
		}
		{
			JButton btnEditar = new JButton("Editar");
			btnEditar.setBounds(343, 94, 81, 23);
			contentPanel.add(btnEditar);
		}
		{
			JButton btnEliminar = new JButton("Eliminar");
			btnEliminar.setBounds(343, 127, 81, 23);
			contentPanel.add(btnEliminar);
		}
		{
			JButton button = new JButton("Importar");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
			button.setBounds(343, 161, 81, 23);
			contentPanel.add(button);
		}
		{
			JButton button = new JButton("Exportar");
			button.setBounds(343, 194, 81, 23);
			contentPanel.add(button);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
