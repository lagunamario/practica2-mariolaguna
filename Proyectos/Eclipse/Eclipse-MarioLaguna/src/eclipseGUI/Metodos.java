package eclipseGUI;

import java.util.Scanner;

import librerias.Operaciones;




public class Metodos {

	public static void main(String[] args) {
		
		Scanner in=new Scanner (System.in);

		int opcion;
		
		System.out.println(" *** MENU ***");
		System.out.println(" 1) Rellenar vector");
		System.out.println(" 2) Visualizar vector");
		System.out.println(" 3) Buscar valor en el vector");
		System.out.println(" 4) Suma dos valores");
		System.out.println("Opcion: ");
		opcion=in.nextInt();
		
		switch (opcion){
		
		case 1: Operaciones.rellenarVector(null);
			break;
			
		case 2: Operaciones.visualizarVector(null);
			break;
			
		case 3: Operaciones.buscarValoresEnVector(null);
			break;
			
		case 4: Operaciones.suma(4, 6);
		
		default: System.out.println("No es un numero correcto.");
		}
	}

}
