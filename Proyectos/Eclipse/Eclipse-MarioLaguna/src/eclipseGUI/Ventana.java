package eclipseGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JPasswordField;

/**
 * 
 * @author Mario
 * @since 16/01/2019
 */
public class Ventana extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtJimor;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Mario\\Pictures\\usb.PNG"));
		setTitle("Registro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo...");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como...");
		mnArchivo.add(mntmGuardarComo);
		
		JMenuItem mntmGuardarTodo = new JMenuItem("Guardar todo");
		mnArchivo.add(mntmGuardarTodo);
		
		JMenu mnEdicion = new JMenu("Edicion");
		menuBar.add(mnEdicion);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEdicion.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEdicion.add(mntmPegar);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnEdicion.add(mntmCortar);
		
		JMenuItem mntmEliminar = new JMenuItem("Eliminar");
		mnEdicion.add(mntmEliminar);
		
		JMenuItem mntmPreferencias = new JMenuItem("Preferencias");
		mnEdicion.add(mntmPreferencias);
		
		JMenu mnFuente = new JMenu("Fuente");
		menuBar.add(mnFuente);
		
		JMenuItem mntmImportar = new JMenuItem("Importar");
		mnFuente.add(mntmImportar);
		
		JMenuItem mntmExportar = new JMenuItem("Exportar");
		mnFuente.add(mntmExportar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmBienvenida = new JMenuItem("Bienvenida");
		mnAyuda.add(mntmBienvenida);
		
		JMenuItem mntmAyudaDeContenidos = new JMenuItem("Ayuda de contenidos");
		mnAyuda.add(mntmAyudaDeContenidos);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnAyuda.add(mntmBuscar);
		
		JMenuItem mntmSobreEclipse = new JMenuItem("Informacion");
		mnAyuda.add(mntmSobreEclipse);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Usuario ", "Usuario invitado", "Administrador", "Superadministrador"}));
		comboBox.setBounds(6, 127, 118, 20);
		contentPane.add(comboBox);
		
		JSlider slider = new JSlider();
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMinimum(-100);
		slider.setMinorTickSpacing(5);
		slider.setMajorTickSpacing(20);
		slider.setBounds(134, 156, 262, 39);
		contentPane.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(118, 70, 49, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(6, 97, 109, 23);
		contentPane.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(114, 97, 109, 23);
		contentPane.add(rdbtnMujer);
		
		JButton btnRestablecer = new JButton("Restablecer");
		btnRestablecer.setBounds(335, 206, 89, 23);
		contentPane.add(btnRestablecer);
		
		JCheckBox chckbxPrivilegios = new JCheckBox("Privilegios");
		chckbxPrivilegios.setBounds(230, 7, 97, 23);
		contentPane.add(chckbxPrivilegios);
		
		JCheckBox chckbxRecordar = new JCheckBox("Recordar");
		chckbxRecordar.setBounds(230, 37, 97, 23);
		contentPane.add(chckbxRecordar);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 11, 101, 14);
		contentPane.add(lblNombre);
		
		txtJimor = new JTextField();
		txtJimor.setForeground(Color.RED);
		txtJimor.setBackground(Color.WHITE);
		txtJimor.setBounds(118, 8, 86, 20);
		contentPane.add(txtJimor);
		txtJimor.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(10, 42, 101, 14);
		contentPane.add(lblPassword);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setBounds(10, 73, 46, 14);
		contentPane.add(lblEdad);
		
		passwordField = new JPasswordField();
		passwordField.setForeground(Color.GREEN);
		passwordField.setBounds(118, 39, 86, 20);
		contentPane.add(passwordField);
		
		JLabel lblSeguridad = new JLabel("Seguridad contrase\u00F1a:");
		lblSeguridad.setBounds(16, 163, 136, 14);
		contentPane.add(lblSeguridad);
	}
}
